﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Routing;

namespace ConferencePlannerTests.Extensions
{
    /// <summary>
    /// Allows for the extension of the Page Context class within the unit test project.
    /// </summary>
    public static class PageContextExtensions
    {
        /// <summary>
        /// Setup the Page Context to contain a Mocked Http Context.
        /// </summary>
        /// <param name="context">Object to call extension method on.</param>
        public static PageContext Mock(this PageContext context)
        {
            var httpContext = new DefaultHttpContext();
            var actionContext = new ActionContext(httpContext, new RouteData(), new PageActionDescriptor(), new ModelStateDictionary());

            return new PageContext(actionContext);
        }
    }
}
