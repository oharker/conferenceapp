﻿using ConferenceDTO;
using ConferencePlanner.Pages;
using ConferencePlanner.Services;
using ConferencePlannerTests.Extensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Authentication;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Routing;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace ConferencePlannerTests
{
    /// <summary>
    /// Tests for the <see cref="IndexCshtmlTests"/> class.
    /// </summary>
    public class IndexCshtmlTests
    {
        /// <summary>
        /// Ensure that when a day number is provided to the <see cref="IndexModel.OnGet(int)"/> method,
        /// the <see cref="IndexModel.CurrentDayOffset"/> property is set.
        /// </summary>
        /// <returns></returns>
        [Fact]
        public async Task OnGet_GivenDay_SetsOffset()
        {
            const int day = 3;

            // Mock the Api Client.
            var apiClient = new Mock<IApiClient>();
            apiClient.Setup(client => client.GetSessionsAsync()).ReturnsAsync(new List<SessionResponse>());
            apiClient.Setup(client => client.GetSessionsByAttendeeAsync(null)).ReturnsAsync(new List<SessionResponse>());

            // Mock the Http Context.
            var pageContext = new PageContext();
            pageContext = pageContext.Mock();
            
            var indexModel = new IndexModel(apiClient.Object)
            {
                PageContext = pageContext
            };
            
            // Call the On Get method within the Index model.
            await indexModel.OnGet(day);

            // Assert Model Current Day Offset is as required.
            Assert.Equal(day, indexModel.CurrentDayOffset);
        }
    }
}
