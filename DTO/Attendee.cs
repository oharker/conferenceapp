﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ConferenceDTO
{
    public class Attendee
    {
        public int ID { get; set; }

        [Required]
        [StringLength(200)]
        [DisplayName("First Name")]
        public virtual string FirstName { get; set; }

        [Required]
        [StringLength(200)]
        [DisplayName("Last Name")]

        public virtual string LastName { get; set; }

        [Required]
        [StringLength(200)]
        public string UserName { get; set; }

        [StringLength(256)]
        public virtual string EmailAddress { get; set; }
    }
}