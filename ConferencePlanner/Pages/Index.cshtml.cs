﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ConferenceDTO;
using ConferencePlanner.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ConferencePlanner.Pages
{
    public class IndexModel : PageModel
    {
        protected readonly IApiClient _apiClient;

        [TempData]
        public string Message { get; set; }

        public bool IsAdmin { get; set; }

        public List<int> UserSessions { get; set; }

        public bool ShowMessage => !string.IsNullOrEmpty(Message);

        public IndexModel(IApiClient apiClient)
        {
            _apiClient = apiClient;
        }

        public IEnumerable<IGrouping<DateTimeOffset?, SessionResponse>> Sessions { get; set; }

        public IEnumerable<(int Offset, DayOfWeek? DayofWeek)> DayOffsets { get; set; }

        public int CurrentDayOffset { get; set; }

        public async Task OnGet(int day = 0)
        {
            CurrentDayOffset = day;

            var sessions = await GetSessionsAsync();

            var userSessions = await _apiClient.GetSessionsByAttendeeAsync(User.Identity.Name);

            var startDate = sessions.Min(s => s.StartTime?.Date);
            var endDate = sessions.Max(s => s.EndTime?.Date);

            var numberOfDays = ((endDate - startDate)?.Days) + 1;

            DayOffsets = Enumerable.Range(0, numberOfDays ?? 0)
                .Select(offset => (offset, (startDate?.AddDays(offset))?.DayOfWeek));

            var filterDate = startDate?.AddDays(day);

            Sessions = sessions.Where(s => s.StartTime?.Date == filterDate)
                               .OrderBy(s => s.TrackId)
                               .GroupBy(s => s.StartTime)
                               .OrderBy(g => g.Key);

            UserSessions = userSessions.Select(s => s.ID).ToList();
        }

        protected virtual Task<List<SessionResponse>> GetSessionsAsync()
        {
            return _apiClient.GetSessionsAsync();
        }

        public async Task<IActionResult> OnPostAsync(int sessionId, int day = 0)
        {
            CurrentDayOffset = 0;
            await _apiClient.AddSessionToAttendeeAsync(User.Identity.Name, sessionId);

            return RedirectToPage(new { day });
        }

        public async Task<IActionResult> OnPostRemoveAsync(int sessionId, int day = 0)
        {
            CurrentDayOffset = day;
            await _apiClient.RemoveSessionFromAttendeeAsync(User.Identity.Name, sessionId);

            return RedirectToPage(new { day });
        }
    }

}
