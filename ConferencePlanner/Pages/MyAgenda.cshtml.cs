using System.Collections.Generic;
using System.Threading.Tasks;
using ConferenceDTO;
using ConferencePlanner.Services;

namespace ConferencePlanner.Pages
{
    public class MyAgendaModel : IndexModel
    {
        public MyAgendaModel(IApiClient client)
            : base(client)
        {

        }
        protected override Task<List<SessionResponse>> GetSessionsAsync()
        {
            return _apiClient.GetSessionsByAttendeeAsync(User.Identity.Name);
        }
    }
}