using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using ConferencePlanner.Filters;
using ConferencePlanner.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ConferencePlanner
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var authBuilder = services
               // .AddAuthentication("Attendee") // This is usually set to Cookies
               .AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/Login";
                    options.AccessDeniedPath = "/Denied";
                });

            // This is changing the Authentication scheme.


            services.AddMvc(options =>
            {
                options.Filters.AddService<RequireLoginFilter>();
            })
            .AddRazorPagesOptions(options =>
            {
                // Authorizes the 'Admin' folder. Protects that view.
                options.Conventions.AuthorizeFolder("/Admin", "Admin");
            });

            var httpClient = new HttpClient
            {
                BaseAddress = new Uri(Configuration["serviceUrl"])
            };
            services.AddSingleton(httpClient);
            services.AddSingleton<IApiClient, ApiClient>();

            // Add Twitter config.
            var twitterConfig = Configuration.GetSection("twitter");
            if (twitterConfig["consumerKey"] != null)
            {
                authBuilder.AddTwitter(options => twitterConfig.Bind(options));
            }

            // Add Google OAuth
            var googleConfig = Configuration.GetSection("google");
            if (googleConfig["clientID"] != null)
            {
                authBuilder.AddGoogle(options => googleConfig.Bind(options));
            }

            // Add admin policy.
            services.AddAuthorization(options =>
            {
                options.AddPolicy("Admin", policy =>
                {
                policy.RequireAuthenticatedUser()
                      .RequireUserName(Configuration["admin"]);
                });
            });

            services.AddTransient<RequireLoginFilter>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
