﻿using ConferencePlanner.Filters;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace ConferencePlanner.Controllers
{
    public class AccountController : Controller
    {
        [HttpPost]
        [SkipWelcome]
        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return Redirect(Url.Page("/Index"));
        }
    }
}
