# README #


### What is this repository for? ###

This repository is practically my playground for all things .NET Core. At the moment it's being used as a reference in case I have to do any of it in the future. 

It iss an almost exact replica of a workshot ran by David Fowler and Damian Edwards at the NDC London conference in January 2018. 

See their [GitHub repository here](https://github.com/DamianEdwards/aspnetcore-app-workshop).

The app was originally intended to show the speaker information at a conference, such as NDC. This app allows each user to log in, and favourite the various sessions available.

This project showcases some of the following features (so far. Totally not copied/hand-held):

* OAuth via Twitter and Google
* RazorPages (Hurray!)
* Entity Framework
* Async testing
* Hand-Built database seeding/hacking
* Tag Helpers

### How do I get set up? ###


* Download Visual Studio 2017
* Install ASP.NET Core 2.0 Tools
* Allow a local database connection
* Ensure that configuration options are correct.
